from datetime import datetime


class Manager:

    def get_user(self):
        args = []
        u = User()
        u.id = args[0]
        u.name = args[1]
        return u

    def save_user(self, user):
        pass
        # user.id = db.get_last_id()
        # save_to_db(user)

    def get_users_projects(self, user):
        return [Project(1, "1233"), Project(2, "3323")]


class SmokingMixin:
    def smoke(self):
        return "{}: Pfff-f-f-f!".format(self.name)


class BaseEntity:
    def __init__(self, id_, name_):
        if isinstance(id_, int) and id_ > 0:
            self.id = id_
        else:
            raise ValueError("Id must be int > 0")

        if isinstance(name_, str) and name_:
            self.name = name_
        else:
            raise ValueError("Name must be not empty string")

        self.date_created = datetime.now()

    def __str__(self):
        return self._prepare_str()

    def _prepare_str(self):
        if self.id and self.name:
            return "{}-{}".format(self.id, self.name)
        if self.id:
            return "{}".format(self.id)
        if self.name:
            return "{}".format(self.name)
        return super(BaseEntity, self).__str__()

    class Meta:
        abstract = True
        db_table = "xz"


class User(SmokingMixin, BaseEntity):
    manager = Manager()
    password = None
    id = None
    name = None

    @property
    def projects(self):
        return self.manager.get_users_projects(self)


class Project(BaseEntity):
    user = None

    def __init__(self, *args, **kwargs):
        if len(args) == 1:
            super(Project, self).__init__(args[0], None)
        elif len(args) == 2:
            super(Project, self).__init__(args[0], args[1])
        elif "id" in kwargs and "name" in kwargs:
            super(Project, self).__init__(kwargs['id'], kwargs['name'])
        elif "id" in kwargs:
            super(Project, self).__init__(kwargs['id'], None)
        elif "name" in kwargs:
            super(Project, self).__init__(None, kwargs['name'])
        else:
            super(Project, self).__init__(None, None)


class Task(BaseEntity):
    project = None

    def __init__(self, *args, **kwargs):
        if len(args) == 1:
            super(Task, self).__init__(args[0], None)
        elif len(args) > 1:
            super(Task, self).__init__(args[0], args[1])
        elif "id" in kwargs and "name" in kwargs:
            super(Task, self).__init__(kwargs['id'], kwargs['name'])
        elif "id" in kwargs:
            super(Task, self).__init__(kwargs['id'], None)
        elif "name" in kwargs:
            super(Task, self).__init__(None, kwargs['name'])
        else:
            super(Task, self).__init__(None, None)
