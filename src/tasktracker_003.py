import sqlite3
import time

conn = sqlite3.connect('db/example.db')
c = conn.cursor()

c.execute('''CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
              name TEXT, password TEXT, creation_date INTEGER)''')

c.execute('''CREATE TABLE IF NOT EXISTS projects (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
              name TEXT, creation_date INTEGER, finished INTEGER, owner_id INTEGER)''')

c.execute('''CREATE TABLE IF NOT EXISTS tasks (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
              name TEXT, creation_date INTEGER, finished INTEGER, parent_project_id)''')


class Manager:
    def get_all_users(self):                            # RC - получить всех пользователей в системе
        c.execute("SELECT name FROM users ")
        print(c.fetchall())
    def get_all_tasks(self):                            # RC - получить все задачи в системе
        c.execute("SELECT name FROM tasks ")
        print(c.fetchall())
    def get_all_projects(self):                         # RC - получить все проекты в системе
        c.execute("SELECT name FROM projects ")
        print(c.fetchall())
    def get_users_projects(self, owner_id):             # RC - получить все проекты пользователя X
        c.execute("SELECT name FROM projects WHERE owner_id=?", (owner_id, ))
        print(c.fetchall())
    def get_projects_tasks(self, parent_project_id):    # RC - получить все задачи проекта Y
        c.execute("SELECT name FROM tasks WHERE parent_project_id=?", (parent_project_id, ))
        print(c.fetchall())
    def get_users_tasks(self, owner_id):                # RC - получить все задачи пользователя X
        c.execute("SELECT id FROM projects WHERE owner_id=?", (owner_id, ))
        project_list = c.fetchall()
        for project_id in project_list:
            c.execute("SELECT name FROM tasks WHERE parent_project_id=?", project_id)
            print(c.fetchall())


class BaseEntity:
    def __init__(self, id_=None, name_=None):
        self.id = id_
        self.name = name_


class User(BaseEntity):
    def __init__(self, id=None, name=None, password_=None):
        self.password = password_
        super().__init__(id, name)

    def create(self, new_name, new_password):
        c.execute("INSERT INTO users (name, password, creation_date) VALUES (?, ?, ?)",
                  (new_name, new_password, time.time()))
        conn.commit()
    def delete(self, id):
        c.execute("DELETE FROM users WHERE id=?", (id, ))
        conn.commit()

    def read(self, user_name):
        c.execute("SELECT * FROM users WHERE name=?", (user_name, ))
        print(c.fetchall())

    def edite(self):
        target_id = input('Enter targets id:  ')
        print('edite name or password?')
        target = input('for choice print "name" or "password":  ')
        if target == 'name':
            new_name = input('input new name:  ')
            c.execute("UPDATE users SET name=? WHERE id=?", (new_name, target_id))
            conn.commit()
        elif target == 'password':
            new_password = input('input new password:  ')
            c.execute("UPDATE users SET password=? WHERE id=?", (new_password, target_id))
            conn.commit()
        else: print('Error')
        print('Done')


class Project(BaseEntity):
    def __init__(self, id=None, name=None, user_=None):
        self.user = user_
        super().__init__(id, name)
    owner_id = None

    def create(self, new_name, owner_id):
        c.execute("INSERT INTO projects (name, creation_date, owner_id) VALUES (?, ?, ?)",
                  (new_name, time.time(), owner_id))
        conn.commit()

    def delete(self, id):
        c.execute("DELETE FROM projects WHERE id=?", (id, ))
        conn.commit()

    def read(self, project_name):
        c.execute("SELECT * FROM projects WHERE name=?", (project_name, ))
        print(c.fetchall())

    def edite(self):
        target_id = input('Enter targets id:  ')
        print('edite name or finished?')
        target = input('for choice print "name" or "finished":  ')
        if target == 'name':
            new_name = input('input new name:  ')
            c.execute("UPDATE projects SET name=? WHERE id=?", (new_name, target_id))
            conn.commit()
        elif target == 'finished':
            f = input('is taget finished? yes/no:  ')
            if f == 'yes':
                c.execute("UPDATE projects SET finished=1 WHERE id=?", (target_id, ))
                conn.commit()
            elif f == 'no':
                c.execute("UPDATE projects SET finished=NULL WHERE id=?", (target_id, ))
                conn.commit()
            else: print('Error')
        else: print('Error')
        print('Done')


class Task(BaseEntity):
    def __init__(self, id=None, name=None, project_=None):
        self.project = project_
        super().__init__(id, name)
    parent_project_id = None

    def create(self, new_name, parent_project_id):
        c.execute("INSERT INTO tasks (name, parent_project_id, creation_date) VALUES (?, ?, ?)",
                  (new_name, parent_project_id, time.time()))
        conn.commit()

    def delete(self, id):
        c.execute("DELETE FROM tasks WHERE id=?", (id, ))
        conn.commit()

    def read(self, project_name):
        c.execute("SELECT * FROM tasks WHERE name=?", (project_name, ))
        print(c.fetchall())

    def edite(self):
        target_id = input('Enter targets id:  ')
        print('edite name or finished?')
        target = input('for choice print "name" or "finished":  ')
        if target == 'name':
            new_name = input('input new name:  ')
            c.execute("UPDATE tasks SET name=? WHERE id=?", (new_name, target_id))
            conn.commit()
        elif target == 'finished':
            f = input('is taget finished? yes/no:  ')
            if f == 'yes':
                c.execute("UPDATE tasks SET finished=1 WHERE id=?", (target_id, ))
                conn.commit()
            elif f == 'no':
                c.execute("UPDATE tasks SET finished=NULL WHERE id=?", (target_id, ))
                conn.commit()
            else: print('Error')
        else: print('Error')
        print('Done')





# u = User()
# u.create('third', 3333)
# u.delete(4)
# u.read('first')
# u.edite()

# p = Project()
# p.create('third_2', 3)
# p.read('first_1')
# p.delete(10)
# p.edite()

t = Task()
# t.create('p9_t1', 9)
t.read('qwerty')
# t.edite()


# m = Manager()
# m.get_all_users()
# m.get_all_projects()
# m.get_all_tasks()
# m.get_users_projects(1)
# m.get_projects_tasks(1)
# m.get_users_tasks(1)